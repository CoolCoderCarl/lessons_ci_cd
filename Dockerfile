FROM python:3.9

WORKDIR /usr/local/bin/

COPY requirements.txt /usr/local/bin/
RUN pip install --no-cache-dir -r requirements.txt

COPY /src/ /usr/local/bin/

CMD ["python", "script.py"]