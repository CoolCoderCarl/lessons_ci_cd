from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello from Docker!"

log_format = "%(levelname)s | %(asctime)s - %(message)s"

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
